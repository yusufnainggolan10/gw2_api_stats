# General
This is data collected via my [Stats Collection Tool](https://gw2.silveress.ie/stats_collection).  
Accuracy is not guaranteed but there is a pretty good chance it is the best around due to how it works.  


## general

```json
[
  83008,
  68340,
  84731,
  68322
]
```

This returns a list of ID's of submitted items

Example  
`https://api.silveress.ie/gw2_stats/v1/general`

Query:

* ~~Beautify~~

## general/id

````json
[
  "Salvaging",
  "Opening"
]
````

Returns a list of activities that are available for that ID.

Example  
`https://api.silveress.ie/gw2_stats/v1/general/69867`


Query:

* ~~Beautify~~


## general/id/activity

````json
[
  {
    "account": "Silveress.5197",
    "wallet": [],
    "items": [
      {
        "id": 64215,
        "quantity": 1,
        "price": 698
      }
    ],
    "consumed": {
      "id": 69867,
      "quantity": -1
    },
    "level": 80,
    "mf": 224,
    "activity": "Opening",
    "salvageKit": "",
    "iso": "2018-10-22T02:55:59.716Z"
  }
]
````

Returns the data for that activity for that ID.  
The time, what was consumed and the output, whether teh wallet had increase or decrease.  
Misc account info such as level and magic-find.

Example  
`https://api.silveress.ie/gw2_stats/v1/general/69867/Opening`


Query:

* Beautify
