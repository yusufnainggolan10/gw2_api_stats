---
title: Silveress's Stats API Documentation

language_tabs: # must be one of https://git.io/vQNgJ
  - json

toc_footers:
  - <a href='https://github.com/lord/slate'>Documentation Powered by Slate</a>

includes:
  - queries
  - forging
  - general

search: true
---

# Introduction

Documentation for my stats API, in case anyone actually wants to use it.  
If ye want it in csv form or for wiki use let me know.

This example API documentation page was created with [Slate](https://github.com/lord/slate). Feel free to edit it and use it as a base for your own API's documentation.

